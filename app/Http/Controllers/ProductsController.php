<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //lista todos los productos en la base de datos
        echo json_encode(Products::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //echo json_encode($request->all());
        //guarda un nuevo producto en la base de datos
        $producto = new Products();
       
        $producto->name = $request->input('name');
        $producto->categories_id = $request->input('categories_id');
        $producto->quantity = $request->input('quantity');
        $producto->save();
    }

    /**
     * Display the specified resource.
     *
     * @param   $products, id especifico del producto que se desea ver
     * @return \Illuminate\Http\Response
     */
    public function show($products)
    {
        //retorna un producto de la base de datos en formato Json
       echo json_encode(Products::find($products));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $products, id del producto a actualizar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $products)
    {
        //se actualiza un producto en especifico por id del producto
        $producto =  Products::find($products);
        $producto->name = $request->input('name');
        $producto->categories_id = $request->input('categories_id');
        $producto->quantity = $request->input('quantity');
        $producto->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $products,id del producto a eliminar
     * @return \Illuminate\Http\Response
     */
    
    public function destroy( $products)
    {
        $producto =  Products::find($products); 
        $producto->delete();
    }
}
