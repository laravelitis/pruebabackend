<?php

namespace App\Http\Controllers;

use App\categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //se listan todas las categorias de la base de datos en formato Json
        echo json_encode(categories::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //se guarda un nuevo registro de categorias en la base de datos 
        $category = new categories();
        $category->name = $request->input('name');
        $category->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  $categories, id de la categoria que se desea ver
     * @return \Illuminate\Http\Response
     */
    public function show( $categories)
    {
        //se lista una categoria en especifico por medio de id categoria en la url
        echo json_encode(categories::find($categories));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit(categories $categories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $categories, id de la categoria a actualizar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categories)
    {
        //se actualiza la categoria especifica por medio de id categoria
        $category = categories::find($categories);
        $category->name = $request->input('name');
        $category->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $categories, id de la categoria a borrar
     * @return \Illuminate\Http\Response
     */
    public function destroy( $categories)
    {
        //se elimina la categoria por medio de id categoria
        $category = categories::find($categories);
        $category->delete();
    }
}
