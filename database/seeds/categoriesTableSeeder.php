<?php

use Illuminate\Database\Seeder;

class categoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => "lacteos",
        ]);
        DB::table('categories')->insert([
            'name' => "carnes",
        ]);
        DB::table('categories')->insert([
            'name' => "granos",
        ]);
        DB::table('categories')->insert([
            'name' => "vegetales",
        ]);
        DB::table('categories')->insert([
            'name' => "dulces",
        ]);
        DB::table('categories')->insert([
            'name' => "panaderia",
        ]);
        DB::table('categories')->insert([
            'name' => "otros",
        ]);
        DB::table('categories')->insert([
            'name' => "atunes",
        ]);
        DB::table('categories')->insert([
            'name' => "embutidos",
        ]);
        DB::table('categories')->insert([
            'name' => "semillas",
        ]);
        DB::table('categories')->insert([
            'name' => "frutas",
        ]);

    }
}
