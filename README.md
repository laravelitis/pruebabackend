# PruebaBackend

Repositorio creado para contener la prueba de admision para el cargo de analista en Man Power

Repositorio en bitbucket 
	git clone https://somarasiel@bitbucket.org/laravelitis/pruebabackend.git

Este proyecto se creo usando el framework de laravel.

Comandos utilizados:

Se crea la aplicacion y se sube la misma al servidor local
- laravel new PruebaBackend
- php artisan serve

Se prooceden a crear los modelos posteriores a migrar 
- php artisan make:model Products -crm
- php artisan make:model categories -crm
- php artisan migrate

Se crean los seeds y se llaman en el archivo databasesSeeders.php
- php artisan make:seeder ProductsTableSeeder
- php artisan make:seeder categoriesTableSeeder
- php artisan db:seed

Se crea relaciones para la tabla de productos y categorias y se comprueban
- php artisan tinker
  $orders = App\categories::find(1)->Products;

Se crean las rutas para el api de los controladores Products y categories y se listan los metodos por los cuales se pueden consumir
- php artisan route:list
- Se utiliza el postman para verificar las diferentes funcionalidades de los api
- Los api se consume por medio de las siguientes url
    productos   = http://127.0.0.1:8000/api/products
    categorias  = http://127.0.0.1:8000/api/categories
- Se configuraron los metodos index, store, show, update, destroy para cada una de las api


Faltantes
- Restricciones a los campos de las bases de datos por medio de laravel, se tiene conocimiento de como hacerlos en la base de datos directamente
- Vistas, pintas los resultados en JavaScript, haciendo uso de los Json retornados por los API

Dificultades

- Se lleva tiempo instalando herramientas de trabajo, git, vscode, laravel, debido a que estas no estaban instadas
- Equipo de computo muy lento
- Se reinicio el equipo de computo y se volvieron a instalar los programas Git y laravel, se empezo a trabajar a las 9:15
- Se tuvo problemas a la hora de crear las relaciones de las tablas en Product y categories por medio de laravel 
